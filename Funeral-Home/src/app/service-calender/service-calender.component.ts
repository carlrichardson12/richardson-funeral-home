import { Component, OnInit } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
@Component({
  selector: 'app-service-calender',
  templateUrl: './service-calender.component.html',
  styleUrls: ['./service-calender.component.css']
})
export class ServiceCalenderComponent implements OnInit {
  title = 'service-calendar';
  calendarPlugins = [dayGridPlugin];
  constructor() { }

  ngOnInit() {
  }

}
