export class Contact {
    fName: string;
    lName: string;
    pNumber: number;
    email: string;
    desc: string;
    contactType: boolean;
    timestamp: Date;
}