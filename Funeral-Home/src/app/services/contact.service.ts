import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contact } from '../models/Contact';

@Injectable({
    providedIn: 'root'
})

export class ContactService {
    private newURL = 'http://localhost:8009/contact/new';
    private getURL = 'http://localhost:8009/contact/all';
    
    constructor(private httpCli: HttpClient){ }
    addContact(c) {
        const eventSuccess = this.httpCli.post<any>(this.newURL, c);
        return eventSuccess;
    }
    getContacts(): Observable<Contact[]> {
        return this.httpCli.get<Contact[]>(this.getURL);
    }
}