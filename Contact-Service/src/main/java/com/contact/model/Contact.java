package com.contact.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@Entity
@Table(name = "Contact")
@EnableEurekaClient
public class Contact {

	@Id
	@Column(name = "Id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "fName")
	private String fName;
	
	@Column(name = "lName")
	private String lName;
	
	@Column(name = "pNumber")
	private String pNumber;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "desc")
	private String desc;
	
	@Column(name = "contactType")
	private String contactType;
	
	@Column(name = "timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date day;
	
	public Contact() {
	}

	public Contact(int id, String fName, String lName, String pNumber, String email, String desc, String contactType,
			Date day) {
		super();
		this.id = id;
		this.fName = fName;
		this.lName = lName;
		this.pNumber = pNumber;
		this.email = email;
		this.desc = desc;
		this.contactType = contactType;
		this.day = day;
	}
	
	public Contact( String fName, String lName, String pNumber, String email, String desc, String contactType,
			Date day) {
		super();
		this.fName = fName;
		this.lName = lName;
		this.pNumber = pNumber;
		this.email = email;
		this.desc = desc;
		this.contactType = contactType;
		this.day = day;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getpNumber() {
		return pNumber;
	}

	public void setpNumber(String pNumber) {
		this.pNumber = pNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String isContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	@Override
	public String toString() {
		return "Contact [fName=" + fName + ", lName=" + lName + ", pNumber=" + pNumber + ", email=" + email + ", desc="
				+ desc + ", contactType=" + contactType + ", day=" + day + "]";
	}
	
	
	
	
	
	
	
	

}
