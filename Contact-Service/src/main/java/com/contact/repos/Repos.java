package com.contact.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.contact.model.Contact;

public interface Repos extends CrudRepository<Contact, String> {
	
	Contact findByfName(String fName);
	Contact findBylName(String lName);
	public List<Contact> findAllByOrderByDay();

	
}
