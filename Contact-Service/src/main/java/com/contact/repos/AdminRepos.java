package com.contact.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.contact.model.Admin;

public interface AdminRepos extends CrudRepository<Admin, String> {
	
		
	//Admin findByemail(String email);
		public List<Admin> findAll();

		
}
