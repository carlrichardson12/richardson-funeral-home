package com.contact.controller;

import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.contact.model.Admin;
import com.contact.repos.AdminRepos;


@CrossOrigin(origins = "*")
@RestController 
@RequestMapping("/admin")  
public class AdminController {
	
	@PersistenceContext
	private EntityManager cMan;
	private AdminRepos data;
	
	@Autowired
	public AdminController (AdminRepos data) {
		this.data = data;
	}

	
	@PostMapping("/new")
	@Transactional
	public Admin  newAdmin( @RequestBody Admin ad) {
		System.out.println(ad);
		data.save(ad);
		return ad;
	}
	
//	@PostMapping("/login")
//	public String login(@RequestBody Admin ad) {
//		String token = new String();
//		Admin person = data.findByemail(ad.getEmail());
//		if(ad.getPassword() == person.getPassword()) {
//			token = randomString();
//		} else {
//			token = new String();
//		}
//		return token;
//	}
	
	public String randomString() {
	    int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 10;
	    Random random = new Random();
	 
	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();
	 
	    return generatedString;
	}
	
}
